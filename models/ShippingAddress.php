<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipping_adress".
 *
 * @property int $id
 * @property int $postcode
 * @property int $country
 * @property string $street
 * @property int $city
 * @property string $house
 * @property string $apartment
 * @property int $status
 * @property int $user_id
 *
 * @property User $user
 */
class ShippingAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipping_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['postcode', 'country', 'street', 'city', 'house'], 'required'],
            [['postcode', 'status', 'user_id'], 'integer'],
            [['street',  'city'], 'string', 'max' => 100],
            [['country',], 'string', 'max' => 2],
            [['house', 'apartment'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'postcode' => 'Postcode',
            'country' => 'Country',
            'street' => 'Street',
            'city' => 'City',
            'house' => 'House',
            'apartment' => 'Apartment',
            'status' => 'Status',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getUserAddresses($user_id)
    {
        return ShippingAddress::findAll(['user_id' => $user_id]);
    }



}
