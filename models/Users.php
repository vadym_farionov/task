<?php

namespace app\models;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $name
 * @property string $sex
 * @property string $created_at
 * @property string $email
 * @property int $status
 * @property string $surname
 * @property object $addresses
 * @property string $updated_at
 * @property string $password
 *
 */
class Users extends \yii\db\ActiveRecord
{

    const FEMALE = 'female';
    const MALE = 'male';
    const UNKNOWN = 'unknown';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'name', 'surname','sex', 'email','password'], 'required'],
            [['created_at','updated_at'], 'safe'],
            [['status'], 'integer'],
            [['login', 'email'], 'string', 'max' => 150],
            [['name','surname'], 'string', 'max' => 100],
            [['sex', 'password'], 'string', 'max' => 50],
            [['email','login'], 'unique'],
            [['login'], 'string', 'min' => 4],
            [['password'], 'string', 'min' => 6],
            [
                ['name', 'surname'],
                \cetver\ValidationFilters\validators\MultibyteUpperCharacterFirstValidator::class,
                'encoding' => 'UTF-8'
            ]
        ];
    }

    public static function sex_list()
    {
        return [
            self::FEMALE => Yii::t('users', 'Female'),
            self::MALE => Yii::t('users', 'Male'),
            self::UNKNOWN => Yii::t('users', 'Unknown'),
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'name' => 'Name',
            'sex' => 'Sex',
            'surname' => 'Surname',
            'created_at' => 'Created At',
            'updated_at' => 'Created At',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'addresses',
                ],
            ],
        ];
    }

//Использовать транзакции для SaveRelationsBehavior
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(ShippingAddress::class, ['user_id' => 'id']);
    }

    public function getAddress($id)
    {

    }

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('users', 'Publish'),
            self::STATUS_INACTIVE => Yii::t('users', 'Unpublish'),
        ];
    }
}
