<?php

namespace app\controllers;

use app\models\ShippingAddress;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use Yii;
use app\models\Users;
use app\models\search\UsersSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 *
 * @property ShippingAddress $shippingAddress
 */
class UsersController extends Controller
{
    use SaveRelationsTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'addreses' => $shipping,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $addresses = $this->getUserAddresses($id);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $addresses,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);
        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $users = new Users();
        $shipping = new ShippingAddress();
        $users->addresses = $shipping;
        if ($users->load(Yii::$app->request->post()) && $shipping->load(Yii::$app->request->post()) && $users->save()) {

            return $this->redirect(['view', 'id' => $users->id]);
        }

        return $this->render('create', [
            'model' => $users,
            'shipping' => $shipping
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $users = $this->findModel($id);
        $shipping = $this->getUserAddresses($id);
        $users->addresses = $shipping;
        if ($users->load(Yii::$app->request->post()) && $users->save()) {
            return $this->redirect(['view', 'id' => $users->id]);
        }

        return $this->render('update', [
            'model' => $users,
            'shipping' => $shipping,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAddAddress($id)
    {
        $address = new ShippingAddress();
        $address->user_id = $id;
        $user = $this->findModel($id);
        if($address->load(Yii::$app->request->post()) && $address->save(false)){
            return $this->redirect(['/users/view', 'id' => $address->user_id]);
        }
        return $this->render('/shipping/create',[
            'model' => $address,
            'user' => $user,
        ]);
    }

    public function actionAddressDelete($id, $user_id)
    {
        ShippingAddress::findOne(['id' => $id])->delete();

        return $this->redirect(['/users/view', 'id' => $user_id]);

    }
    public function actionAddressUpdate($id, $user_id)
    {
        $address = ShippingAddress::findOne($id);
        $user = $this->findModel($user_id);
        if ($address->load(Yii::$app->request->post()) && $address->save(false)) {
            return $this->redirect(['view', 'id' => $user_id]);
        }
        return $this->render('/shipping/update', [
            'user' => $user,
            'model' => $address,
        ]);

    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function getUserAddresses($user_id)
    {
        $shipping = new ShippingAddress();
        return $shipping->getUserAddresses($user_id);
    }


}
