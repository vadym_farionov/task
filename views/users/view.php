<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $dataProvider app\models\ShippingAddress */

$this->title = $model->name . ' ' . $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-method' => 'POST']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'login',
            'name',
            'surname',
            'sex',
            [
                    'attribute' => 'created_at',
                    'format' => ['datetime', 'php:d-m-Y g:i a'],
            ],
            'email:email',
            [
                'label' => 'Status',
                'value' => function ($model) {
                    return \app\models\Users::getStatuses()[$model->status];
                }
            ]
        ],
    ]) ?>
    <h2><?= Yii::t('users', 'Addresses List') ?></h2>
    <?= Html::a('<span class="glyphicon glyphicon-plus" style="margin-right: 0.5em;"></span>Add', ['/users/add-address', 'id' => $model->id], ['data' => ['method' => 'POST'], 'class' => 'btn btn-info']); ?>
    <div class="table-responsive"><?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'tableOptions' => ['class' => 'table table-striped table-bordered'],
            'columns' => [
                    'id',
                'postcode',
                'country',
                'street',
                'city',
                'house',
                'apartment',
                [
                    'label' => 'status',
                    'value' => function ($dataProvider) {
                        return \app\models\Users::getStatuses()[$dataProvider->status];
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Remove/Update',
                    'template' => '{address-update} {address-delete}',
                    'buttons' => [
                        'address-update' => function ($url,$dataProvider) {
                            $url = '/users/address-update?id='.$dataProvider->id.'&user_id='.$dataProvider->user_id;
                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', [$url]);
                        },
                        'address-delete' => function ($url, $dataProvider) {
                            $url = '/users/address-delete?id='.$dataProvider->id.'&user_id='.$dataProvider->user_id;
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>', [$url],['data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                                    ]);
                        },

                    ],

                ],
            ]
        ]);
        ?>
    </div>
</div>
