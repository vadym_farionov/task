<?php

/**
 *
 * @var $model \app\models\Users
 */

use yii\helpers\Html; ?>
<li class="list-group-item">
    <div class="checkbox">
        <?= Html::a($model->name,\yii\helpers\Url::to(['view', 'id' => $model->id]))?>
    </div>
    <div class="pull-right action-buttons">
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>',\yii\helpers\Url::to(['update', 'id' => $model->id]),['data-method' => 'POST'])?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>',\yii\helpers\Url::to(['delete', 'id' => $model->id]),['class' => 'trash','data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],])?>

    </div>
</li>
