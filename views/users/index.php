<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-plus" style="margin-right: 0.5em;"></span>Create User', ['create'], ['class' => 'btn btn-success', 'data-method' => 'POST']) ?>
    </p>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <?= Yii::t('users', 'Users List') ?>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <?= ListView::widget([
                                'dataProvider' => $dataProvider,
                                'itemOptions' => ['class' => 'item'],
                                'itemView' => 'list_view',
                                'summary' => '',
                                'pager' => [
                                    'pagination' => $dataProvider->setPagination(['pageSize' => 5]),
                                ]

                            ]) ?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
