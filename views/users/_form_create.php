<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
/* @var $shipping \app\models\ShippingAddress*/
?>

<div class="box-body table-responsive">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active">
            <a data-toggle="tab" href="#user" href="#">
                <?= Yii::t('users', 'User'); ?>
            </a>
        </li>
        <li role="presentation">
            <a data-toggle="tab" href="#shipping" href="#">
                <?= Yii::t('users', 'Shipping'); ?>
            </a>
        </li>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="user">
            <div class="box-body">

<?// var_dump($model)?>
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'sex')->dropDownList(\app\models\Users::sex_list()) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'status')->dropDownList(\app\models\Users::getStatuses()) ?>


            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="shipping">
            <div class="box-body">
                <?= $form->field($shipping, 'postcode')->textInput(['maxlength' => true]) ?>

                <?= $form->field($shipping, 'country')->textInput(['maxlength' => true]) ?>

                <?= $form->field($shipping, 'city')->textInput(['maxlength' => true]) ?>

                <?= $form->field($shipping, 'street')->textInput() ?>

                <?= $form->field($shipping, 'house')->textInput(['maxlength' => true]) ?>

                <?= $form->field($shipping, 'apartment')->textInput() ?>

                <?= $form->field($shipping, 'status')->dropDownList(\app\models\Users::getStatuses()) ?>




            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
