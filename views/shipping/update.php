<?php

use app\models\ShippingAddress;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ShippingAddress */
/* @var $form ActiveForm */
?>
<h1><?= Yii::t('users', 'Shipping update for'.' '. $user->name)?></h1>
<div class="shipping-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street')->textInput() ?>

    <?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apartment')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\Users::getStatuses()) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('users', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- shipping-create -->